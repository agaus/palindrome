require "agaus_palindrome/version"

class String

  def palindrome?
    processed_content == processed_content.reverse
  end

  private

    def processed_content
      self.scan(/\w/).join.downcase
    end
end
